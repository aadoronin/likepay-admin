/* eslint-disable */
import store from '../store/index'

import Login from '../pages/unauth/Login'
import AdminDashboard from '../pages/auth/admin/dashboard/Index'
import AdminInvite from '../pages/auth/admin/invite/Index'
import AdminClients from '../pages/auth/admin/clients/Index'
import AdminClient from '../pages/auth/admin/clients/Client'

const ifAdmin = (to, from, resolve, reject) => {
  console.log('store.getters.role', store.getters.role)
  if (store.getters.isAuthenticated && store.getters.getRole === 'admin') {
    resolve()
  } else {
    reject('/login')
  }
}

export default [
  {
    path: '/',
    name: 'Login',
    alias: '/login',
    component: Login
  },
  {
    path: '/admin/dashboard',
    name: 'AdminDashboard',
    component: AdminDashboard,
    beforeEnter: ifAdmin
  },
  {
    path: '/admin/invite',
    name: 'AdminInvite',
    component: AdminInvite,
    beforeEnter: ifAdmin
  },
  {
    path: '/admin/clients',
    name: 'AdminClients',
    component: AdminClients,
    beforeEnter: ifAdmin
  },
  {
    path: '/admin/client/:uuid/',
    name: 'AdminClient',
    component: AdminClient,
    beforeEnter: ifAdmin
  }
]
