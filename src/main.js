import Vue from 'vue'
import App from './App.vue'
import store from './store'
import Framework7 from 'framework7/framework7.esm.bundle.js'
import Framework7Vue from 'framework7-vue/framework7-vue.esm.bundle.js'
import 'framework7/css/framework7.bundle.css'
import 'framework7-icons/css/framework7-icons.css'
import 'material-design-icons/iconfont/material-icons.css'

import i18n from './i18n/index'

Vue.config.productionTip = false

Framework7.use(Framework7Vue)

new Vue({
  i18n,
  store,
  render: h => h(App)
}).$mount('#app')
