/* eslint-disable */

export default {
  login: 'Login',
  logout: 'Logout',
  invite: {
    create: 'Create new',
    code: 'Invitation code',
    expired: 'Expires',
    createTitle: 'Create new invite',
    list: 'List of invite',
    fee: 'Fee',
  },
  pages: {
    invite: {
      name: 'Invite',
    },
  },
  copy_to_clipboard: 'copy',
  password: 'Password',
  username: 'Username',
  dashboard: 'Dashboard',
  table: {
    of: 'of',
    perPage: 'Per page'
  },
  clients: {
    tItle: 'Clients'
  },
  'dashboard.text': 'A little later there will be useful information, but for now you can create an invite',

}
