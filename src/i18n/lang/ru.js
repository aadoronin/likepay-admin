/* eslint-disable */

export default {
  'dashboard.text': 'Чуть позже тут появится полезная информация, а пока что можете создать инвайт',
  login: 'Вход',
  logout: 'Выход',
  invite: {
    create: 'Новый',
    code: 'Инвайт код',
    expired: 'Истекает',
    createTitle: 'Создание нового приглашения',
    list: 'Список приглашений',
    fee: 'Комиссия'
  },
  pages: {
    invite: {
      name: 'Приглашения'
    }
  },
  copy_to_clipboard: 'скопировать',
  dashboard: 'Главная',
  table: {
    of: 'из',
    perPage: 'Отображать на странице'
  },
  clients: {
    tItle: 'Клиенты'
  },
  username: 'Имя пользователя',
  password: 'Пароль'
}
