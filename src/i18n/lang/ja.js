/* eslint-disable */

export default {
  login: 'ユーザー名',
  logout: 'ログアウト',
  invite: {
    create: '作成',
    code: '招待コード',
    expired: '有効期限',
    createTitle: '招待コードを発行',
    list: '招待コードのリスト',
    fee: '手数料',
  },
  pages: {
    invite: {
      name: '招待',
    },
  },
  copy_to_clipboard: 'コピー',
  password: 'パスワード',
  username: 'ユーザー名',
  dashboard: 'ホーム',
  table: {
    of: 'から',
    perPage: 'ページで表示',
  },
  clients: {
    tItle: 'クライアント',
  },
}
