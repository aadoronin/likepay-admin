import Vue from 'vue'
import VueI18n from 'vue-i18n'
import en from './lang/en'
import ru from './lang/ru'
import ja from './lang/ja'

Vue.use(VueI18n)

let messages = {
  en,
  ru,
  ja
}

export default new VueI18n({
  locale: window.localStorage.language || 'en',

  fallbackLocale: 'ru',
  messages
})
