import moment from 'moment'
import Decimal from 'decimal'

/**
 * @param {Date|Moment|String|Number} date
 * @param {String} [format]
 * @return {String}
 */
export function formatDate (date, format = 'DD.MM.YYYY') {
  return moment(date).format(format)
}
/**
 * @param {String|Number} any
 * @return {Decimal}
 */
export function decimal (any) {
  return new Decimal(any)
}
/**
 * @param {Object} plainObject
 * @returns {Object}
 */
export function copyAsJson (plainObject) {
  return JSON.parse(JSON.stringify(plainObject))
}
