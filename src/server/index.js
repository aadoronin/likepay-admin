
const origin = process.env.VUE_APP_API_PATH

export default {
  authLogin: {
    url: `${origin}/auth/login`,
    method: 'POST',
    dataType: 'json',
    contentType: 'application/json',
    headers: {
      'X-Requested-With': 'X-LP-SessionManager'
    },
    data: {}
  },
  inviteList: {
    url: `${origin}/invites`,
    method: 'GET',
    headers: {
      'X-Requested-With': 'X-LP-SessionManager'
    },
    data: {}
  },
  inviteNew: {
    url: `${origin}/invites`,
    method: 'POST',
    dataType: 'json',
    contentType: 'application/json',
    headers: {
      'X-Requested-With': 'X-LP-SessionManager'
    },
    data: {}
  },
  clientList: {
    url: `${origin}/clients`,
    method: 'GET',
    headers: {
      'X-Requested-With': 'X-LP-SessionManager'
    },
    data: {}
  },
  clientSave: {
    url: `${origin}/clients/current`,
    method: 'PUT',
    dataType: 'json',
    contentType: 'application/json',
    headers: {
      'X-Requested-With': 'X-LP-SessionManager'
    },
    data: {}
  },
  clientApproved: {
    url: `${origin}/clients/approved`,
    method: 'GET',
    headers: {
      'X-Requested-With': 'X-LP-SessionManager'
    },
    data: {}
  },
  getCountries: {
    url: `${origin}/references/countries`,
    method: 'GET',
    headers: {
      'X-Requested-With': 'X-LP-SessionManager'
    },
    data: {}
  },
  getCities: {
    url: `${origin}/references/cities`,
    method: 'GET',
    headers: {
      'X-Requested-With': 'X-LP-SessionManager'
    },
    data: {}
  },
  getClientTypes: {
    url: `${origin}/references/client_types`,
    method: 'GET',
    headers: {
      'X-Requested-With': 'X-LP-SessionManager'
    },
    data: {}
  }
}
