/* eslint-disable */

import axios from 'axios'
import { AUTH_REQUEST, AUTH_ERROR, AUTH_SUCCESS, AUTH_LOGOUT } from '../actions/auth'
import { USER_REQUEST } from '../actions/user'
import { ERROR_ADD } from '../actions/error'
import server from '../../server/index'
import Framework7 from '../../server/index'

const state = {
  token: sessionStorage.getItem('user-token') || '',
  status: '',
  hasLoadedOnce: false,
  pending: false
}

const getters = {
  isAuthenticated: state => !!state.token,
  authStatus: state => state.status,
  authPending: state => state.pending
}

const actions = {
  [AUTH_REQUEST]: ({ commit, dispatch }, data) => {
    return new Promise((resolve, reject) => {
      commit(AUTH_REQUEST)

      const authLogin = server.authLogin
      authLogin.data = Object.assign(authLogin.data, data.data)

      data.request.promise(authLogin)
        .then(resp => {
          console.log('login response', resp)
          return resp.meta.code === 0
            ? Promise.resolve(resp)
            : Promise.reject(resp)
        })
        .then(resp => {
          const data = resp.data

          sessionStorage.setItem('user-token', data.session)
          axios.defaults.headers.common['Authorization'] = data.session
          commit(AUTH_SUCCESS, data)
          dispatch(USER_REQUEST)
          resolve(data)
        })
        .catch(resp => {
          console.log('auth/login err', resp.meta.message)

          commit(AUTH_ERROR, resp)
          dispatch(ERROR_ADD, resp.meta)
          sessionStorage.removeItem('user-token')
          reject(resp)
        })
    })
  },
  [AUTH_LOGOUT]: ({ commit, dispatch }) => {
    return new Promise((resolve, reject) => {
      commit(AUTH_LOGOUT)
      sessionStorage.removeItem('user-token')
      sessionStorage.removeItem('profile')
      return resolve()
    })
  }
}

const mutations = {
  [AUTH_REQUEST]: (state) => {
    state.status = 'loading'
    state.pending = true
  },
  [AUTH_SUCCESS]: (state, resp) => {
    state.status = 'success'
    state.token = resp.session
    state.hasLoadedOnce = true
    state.pending = false
  },
  [AUTH_ERROR]: (state) => {
    state.status = 'error'
    state.hasLoadedOnce = true
    state.pending = false
  },
  [AUTH_LOGOUT]: (state) => {
    state.token = ''
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
