/* eslint-disable */

import { USER_REQUEST, USER_ERROR, USER_SUCCESS, USER_CHANGE_LANGUAGE } from '../actions/user'
import Vue from 'vue'
import axios from 'axios'
import { AUTH_LOGOUT, AUTH_REQUEST, AUTH_SUCCESS } from '../actions/auth'
import {version} from '../../../package'

const state = {
  status: '',
  profile: JSON.parse(sessionStorage.getItem('profile')) || {},
  language: localStorage.getItem('language') || 'en',
  languages: [
    {code: 'ja', name: '日本語', alpha_2: 'ja'},
    {code: 'en', name: 'English', alpha_2: 'en'},
    {code: 'ru', name: 'Русский', alpha_2: 'ru'},
  ],
  userAppVersion: version || ''
}

const getters = {
  getProfile: state => state.profile,
  getRole: state => state.profile.role,
  isProfileLoaded: state => !!state.profile.name,
  getLang: state => state.language
}

const actions = {
  [USER_REQUEST]: ({ commit, dispatch }) => {

    return new Promise((resolve, reject) => {
      commit(USER_REQUEST)
      // axios({ url: 'user/me' })
      //   .then(resp => {
      //     commit(USER_SUCCESS, resp)
      //   })
      //   .catch(resp => {
      //     commit(USER_ERROR)
      //     // if resp is unauthorized, logout, to
      //     dispatch(AUTH_LOGOUT)
      //   })
      const response = {
        name: 'Alex',
        role: 'admin'
      }
      sessionStorage.setItem('profile',  JSON.stringify(response))
      commit(USER_SUCCESS, response)
      console.log('user resolve')
      resolve(response)
    })
  },

  [USER_CHANGE_LANGUAGE]: ({ commit, dispatch }, lang) => {

    localStorage.setItem('language',  lang)
    commit(USER_CHANGE_LANGUAGE, lang)
  }
}

const mutations = {
  [USER_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [USER_SUCCESS]: (state, resp) => {
    state.status = 'success'
    Vue.set(state, 'profile', resp)
  },
  [USER_ERROR]: (state) => {
    state.status = 'error'
  },
  [AUTH_LOGOUT]: (state) => {
    state.profile = {}
  },
  [USER_CHANGE_LANGUAGE]: (state, lang) => {
    state.language = lang
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
