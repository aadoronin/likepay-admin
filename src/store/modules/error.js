/* eslint-disable */

import { ERROR_ADD, ERROR_DELETE } from '../actions/error'

const state = {
  message: ''
}

const getters = {
  isError: state => state.message.length > 0,
}

const actions = {
  [ERROR_ADD]: ({ commit, dispatch }, error) => {
    console.log('error actions add', error)
    commit(ERROR_ADD, error)
  },
  [ERROR_DELETE]: ({ commit, dispatch }) => {
    commit(ERROR_DELETE)
  }
}

const mutations = {
  [ERROR_ADD]: (state, err) => {
    console.log('error mutations add', err)
    state.message = err.message
  },
  [ERROR_DELETE]: (state) => {
    state.message = ''
  },
}

export default {
  state,
  getters,
  actions,
  mutations
}
