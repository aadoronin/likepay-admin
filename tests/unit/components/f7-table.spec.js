import { assert } from 'chai'
import { shallowMount } from '../../../node_modules/@vue/test-utils'
import f7Table from '../../../src/components/f7-table'

describe('components/f7-table', () => {
  it('renders props.msg when passed', () => {
    const wrapper = shallowMount(f7Table, {
      propsData: {
        fields: [
          {
            name: 'code',
            title: 'code',
            titleClass: 'label-cell',
            dataClass: 'numeric-cell',
            sortTable: 'Ob'
          }
        ],
        querydata: {}
      },
      mocks: {
        $t: () => 'some specific text'
      }
    })
    assert(wrapper.html())
  })
})
